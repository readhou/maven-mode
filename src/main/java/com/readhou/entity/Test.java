package com.readhou.entity;

import java.util.Objects;

/**
 * @author Houjk
 * @version 1.0
 * @date 2022/8/24 20:30
 * @description
 */



public class Test {
    private String id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Test test = (Test) o;
        return Objects.equals(id, test.id) && Objects.equals(name, test.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }




}
