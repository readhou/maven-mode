package com.readhou.util.file;

import cn.hutool.core.util.XmlUtil;
import com.readhou.common.path.PathUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Houjk
 * @version 1.0
 * @date 2022/6/21 22:08
 * @description
 */
public class createXml {

    private final static String XML = "/xml/";

    public static void main(String[] args) {
        Document document = XmlUtil.createXml();
        document.setXmlStandalone(false);  //不显示standalone="no"
        Element root = document.createElement("ROOT");
        Element hearder = document.createElement("hearder");
        //用户名
        Element username = document.createElement("username");
        username.setTextContent("admin");
        //密码
        Element password = document.createElement("username");
        password.setTextContent("123456");
        //数据层
        Element data = document.createElement("data");
        //表信息
        for (int x = 0; x < 2; x++) {
            Element tables = document.createElement("table");
            tables.setAttribute("tableName", "qbmb");
            tables.setAttribute("chinaName", "情报目标");
            //循环表字段
            for (int i = 0; i < 10; i++) {
                Element zd = document.createElement("zd");
                zd.setAttribute("zdname", "id");
                zd.setAttribute("zdtype", "varchar2");
                zd.setAttribute("zdcd", "5");
                zd.setAttribute("zdjd", "2");
                zd.setAttribute("zdsm", "说明");
                zd.setAttribute("sfzj", "1");
                tables.appendChild(zd);
            }
            data.appendChild(tables);
        }
        hearder.appendChild(username);
        hearder.appendChild(password);
        root.appendChild(hearder);
        root.appendChild(data);
        document.appendChild(root);
        XmlUtil.toFile(document, PathUtils.getAppPath() + XML + "file.xml", "UTF-8");
    }
}
