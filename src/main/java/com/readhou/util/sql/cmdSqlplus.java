package com.readhou.util.sql;

import com.readhou.common.path.PathUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Houjk
 * @version 1.0
 * @date 2022/7/30 23:57
 * @description 通过java调用cmd调用sqlplus调用sql文件可包含非sql语句（如：过程）
 * 如果要携带参数 &1 &2 ...
 * 解决思路：通过java代码优先进行过滤生成新的sql文件然后再通过本方法执行
 */
public class cmdSqlplus {

    private final static String SQLPLUS = "/sqlplus/";

    public static void main(String[] args) {
        String username = "test";
        String password = "test";
        String ip = "192.168.0.101";
        String dk = "1521";
        String slm = "orcl";
        String filename = "test.sql";
        String cmdPath = PathUtils.getAppPath() + SQLPLUS;
        PathUtils.mkdir(cmdPath);
        String logname = cmdPath + "test.log";
        cmdSqlPlus(username, password, ip, dk, slm, filename, logname, cmdPath);
    }

    public static void cmdSqlPlus(String username,
                                  String password,
                                  String ip,
                                  String dk,
                                  String slm,
                                  String fileName,
                                  String logName,
                                  String cmdPath) {
        FileOutputStream fos = null;
        InputStream in = null;
        Process pr;
        try {
            StringBuilder cmd = new StringBuilder("sqlplus ");
            cmd.append(username)
                    .append("/")
                    .append(password)
                    .append("@")
                    .append(ip)
                    .append(":")
                    .append(dk)
                    .append("/")
                    .append(slm)
                    .append(" @ ")
                    .append(fileName);
            String cmdstr = cmd.toString();
            System.out.println(cmd.toString());
            Runtime runtime = Runtime.getRuntime();
            System.out.println(cmdPath);
            pr = runtime.exec(cmdstr, null, new File(cmdPath));
            in = pr.getInputStream();
            fos = new FileOutputStream(logName);
            byte[] b = new byte[1024];
            int br;
            while ((br = in.read(b)) != -1) {
                String str = new String(b, 0, br);
//                int i = str.indexOf("SP2-0310");
//                if (i != -1) {
//                    pr.destroy();
//                    System.out.println("执行中断:" + str);
//                }
                int j = str.indexOf("SQL>");
                if (j != -1) {
                    pr.destroy();
                    System.out.println("成功执行");
                }
                fos.write(b, 0, br);
            }
//            pr.waitFor();
            fos.flush();
            fos.close();
            in.close();
            pr.destroy();
            System.out.println("结束");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
