package com.readhou.common.path;

import cn.hutool.core.io.file.PathUtil;

import java.nio.file.Paths;

/**
 * @author Houjk
 * @version 1.0
 * @date 2022/7/30 22:57
 * @description
 */
public class PathUtils extends PathUtil {

    private final static String BEFORE = "/data";


    public static String getAppPath(){
       return System.getProperty("user.dir")+BEFORE;
    }


    /**
     * 判断路径是否存在不存在则会创建
     * @param path 路径
     */
    public static void mkdir(String path){
        PathUtil.mkdir(Paths.get(path));
    }



}
